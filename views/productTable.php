<?php $products=require_once('products.php');
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Produits</title>
</head>
<body>
    <h1>Liste des produits</h1>
    <?php foreach ($products as $value): ?>
    <table>
        <thead>
            <tr>
                <th><?= $value-> getId()?></th>
                <th><?= $value-> getName()?></th>
                <th><?= $value-> getPrice()?></th>
                <th><?=(method_exists($value, "getBrand")) ?$value->getBrand():null?></th>
                <th><?=(method_exists($value, "getProductorName")) ?$value->getProductorName():null?></th>
                <th><?=(method_exists($value, "ExpireAt")) ?$value->ExpireAt():null?></th>
            </tr>
        </thead>
    </table>
<?php endforeach; ?>

</body>
</html>