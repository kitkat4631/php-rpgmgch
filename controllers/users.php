<?php
require ('Client.php');

$client1 = new Client('id 1', 'kitkat4631@gmail.com', date('d-m-y', time()));
$client2 = new Client('id 2', 'client@gmail.com', date('d-m-y', time()));

$arrayClients = [
    1 => $client1,
    2 => $client2
];

return $arrayClients;
?>